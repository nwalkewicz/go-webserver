package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path"
	"strconv"
)

func printUsage() {
	fmt.Fprintf(os.Stderr, "Usage: %s [-p PORT] [directory]\n", os.Args[0])
}

func main() {
	port := flag.Int("p", 8000, "Port number")
	flag.Usage = func() {
		printUsage()
		flag.PrintDefaults()
	}

	flag.Parse()

	directory := "."
	if flag.NArg() > 0 {
		directory = path.Clean(flag.Arg(0))
	}

	handler := http.FileServer(http.Dir(directory))
	portNum := ":" + strconv.Itoa(*port)

	go func() {
		err := http.ListenAndServe(portNum, handler)
		if err != nil {
			fmt.Println("Couldn't start the server:", err)
			return
		}
	}()

	fmt.Println("Server listening at " + portNum)

	select {}
}
