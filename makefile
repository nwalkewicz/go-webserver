ifeq ($(OS),Windows_NT)
	OUTPUT_NAME = build/srv.exe
else
	OUTPUT_NAME = build/srv
endif

default:
	go build -o $(OUTPUT_NAME)

all: win linux

win:
	GOOS=windows GOARCH=amd64 go build -o build/srv.exe

linux:
	GOOS=linux GOARCH=amd64 go build -o build/srv

clean:
	rm -f build/srv build/srv.exe
